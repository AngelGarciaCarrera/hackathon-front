import { LitElement, html } from "lit-element";
import '../producto-main/producto-main.js';
import '../producto-sidebar/producto-sidebar.js';
import '../test-api/test-api.js';


class ProductosApp extends LitElement {
  static get properties() {
    return {
      productos: { type: Array }
    };
  }
  constructor() {
    super();
  }
  render() {
    return html`
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
      <productos-header></prodctos-header>
      <div class="row">
        <producto-sidebar class="col-2"></producto-sidebar>
        <producto-main class="col-10"></producto-main>
      </div>
      `;
  }
}

customElements.define('productos-app', ProductosApp);
