import { LitElement, html } from "lit-element";
import '../producto-ficha-listado/producto-ficha-listado.js';
import '../test-api/test-api.js';

class ProductoMain extends LitElement {
  static get properties() {
    return {
      productos: { type: Array },
      data: { type: Object }
    };

  }
  constructor() {
    super();
    this.productos = [];
  }
  render() {
    return html`
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
      <h2 class ="text-center">Productos</h2>
      <div class="row" id="productosList">
        <div class="row row-cols-1 row-cols-sm-4">
        ${this.productos.map(
      producto => html`<producto-ficha-listado
              idUsuario = "${producto.idUsuario}"
              nameUsuario = "${producto.nameUsuario}"
              idProducto ="${producto.idProducto}"
              descProducto = "${producto.descProducto}"
              importe =  "${producto.importe}"
               @delete-product="${this.deleteProduct}"
               @info-product="${this.infoProduct}"
              ></producto-ficha-listado>`
    )}
        </div>
      </div>
      <test-api
          @productos-data-get="${this.productosDataGet}"
      ></test-api>
    `;
  }
  updated(changedProperties) {
    console.log("updated");

    if (changedProperties.has("productos")) {
      console.log("Ha cambiado el valor de la propiedad productos en producto-main");
      this.dispatchEvent(
        new CustomEvent(
          "updated-product",
          {
            detail: {
              idUsuario: this.idUsuario,
              nameUsuario: this.nameUsuario,
              idProducto: this.idProducto,
              descProducto: this.descProducto,
              importe: this.importe

            }
          }
        )
      )
    }

  }

  productosDataGet(e) {
    console.log("productosDataUpdated");
    console.log(e.detail);
    console.log(e.detail.productos);
    this.productos = e.detail.productos;
  }
  deleteProduct(e) {

    console.log("deleteProduct en producto-main");
    console.log("Se va a borrar el producto " + e.detail.idProducto + " del usuario " + e.detail.idUsuario + e.detail.nameUsuario + e.detail.descProducto);

    this.productos = this.productos.filter(
      clave => (clave.idProducto != e.detail.idProducto && clave.idUsuario != e.detail.idUsuario)

    );
    console.log("eliminando el producto con el valor " + e.detail.idUsuario + "....." + e.detail.idProducto);
    console.log("prueba");
    //deleteproductb();
    console.log("deleteProduct");
    //const nodeFetch = require('node-fetch');
    let xhr = new XMLHttpRequest();
    let data = {
      "idUsuario": e.detail.idUsuario,
      "nameUsuario": e.detail.nameUsuario,
      "idProducto": e.detail.idProducto,
      "descProducto": e.detail.descProducto,
      "importe": e.detail.importe

    };
    console.log(JSON.stringify(data));
    var url = "http://localhost:8080/hackathon/prestamos/";

    xhr.open("DELETE", url);//+ '/' + e.detail.idUsuario + '/' + e.detail.idProducto, true);
    xhr.onload = function () {
      if (xhr.status === 200) {
        console.log("Peticion completada correctamente");
      }
      console.log(data);
      console.log(JSON.stringify(data));


    }
    xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    xhr.send(JSON.stringify(data));

  }
  infoProduct(e) {
    console.log("infoProduct");
    console.log("Se ha pedido más información del producto " + e.detail.nameUsuario + ".." + e.detail.descProducto);

    let choosenProduct = this.productos.filter(
      product => (product.idProducto != e.detail.idProducto && product.idUsuario != e.detail.idUsuario)
    );
    //console.log(choosenPerson);
    //console.log(choosenPerson[0].name); acceder al nombre (primer elemento del array)
    let product = {};
    product.idUsuario = choosenProduct[0].idUsuario;
    product.nameUsuario = choosenProduct[0].nameUsuario;
    product.idProducto = choosenProduct[0].idProducto;
    product.descProducto = choosenProduct[0].descProducto;
    product.importe = choosenProduct[0].importe;
    this.shadowRoot.getElementById("productForm").product = product;
    this.shadowRoot.getElementById("productForm").editingProduct = true;
    this.showPersonForm = true;
  }
}

customElements.define('producto-main', ProductoMain);
