import { LitElement, html } from "lit-element";

class ProductoFichaListado extends LitElement {
  static get properties() {
    return {
      idUsuario: { tye: String },
      nameUsuario: { tye: String },
      idProducto: { tye: String },
      descProducto: { tye: String },
      importe: { tye: Number }
    };
  }
  constructor() {
    super();
    //this.nameUsuario = "Pedro";

  }
  render() {
    return html`
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <div class="card h-100">
      <div class="card-body">
        <h5 class="card-title">${this.idUsuario} : ${this.nameUsuario}</h5>
        <p class="card-text">${this.idProducto} : ${this.descProducto}</p>
        <ul class="list-group list-group-flush">
          <li class="list-group-item">${this.importe} € </li>
        </ul>
      </div>
 <div class="card-footer">
        <button @click="${this.deleteProduct}"class ="btn btn-danger col-5"><strong>X</strong></button>
        <button @click="${this.moreInfo}"class ="btn btn-info col-5 offset-1"><strong>Info</strong></button>
      </div>
    </div>
    `;
  }
  deleteProduct(e) {
    console.log("deleteProduct en producto-ficha-listado");
    console.log("Se va a borrar el producto  " + this.descProducto + " del usuario " + this.nameUsuario);

    this.dispatchEvent(
      new CustomEvent(
        "delete-product",
        {
          "detail": {
            idUsuario: this.idUsuario,
            nameUsuario: this.nameUsuario,
            idProducto: this.idProducto,
            descProducto: this.descProducto,
            importe: this.importe
          }
        }
      )
    )
  }
  moreInfo() {
    console.log("moreInfo");
    console.log("Se ha pedido mas información delproducto ");

    this.dispatchEvent(
      new CustomEvent(
        "info-product",
        {
          "detail": {
            idUsuario: this.idUsuario,
            nameUsuario: this.nameUsuario,
            idProducto: this.idProducto,
            descProducto: this.descProducto,
            importe: this.importe
          }
        }

      )
    )
  }
}

customElements.define('producto-ficha-listado', ProductoFichaListado);
