import { LitElement, html } from "lit-element";

class TestAPI extends LitElement {
  static get properties() {
    return {
      productos: { type: Array },
      clave: { type: Object }
    };
  }
  constructor() {
    super();
    console.log("Arranca el test api");
    // this.clave = {
    //   idProducto: this.idProducto,
    //   idUsuario: this.idUsuario
    // }
    //  console.log(this.clave);
    this.productos = [];
    this.getproduct();
    //this.pushproduct();

  }

  updated(changedProperties) {
    console.log("updated");
    if (changedProperties.has("productos")) {
      console.log("Actualizada la propiedad productos");
      this.dispatchEvent(
        new CustomEvent(
          "productos-data-get",
          {
            detail: {
              productos: this.productos
            }
          }
        )
      )
    }
    if (changedProperties.has("idUsuario")) {
      console.log("Actualizada la propiedad clave");
      this.deleteproduct();
    }

  }
  getproduct() {
    console.log("getProduct");
    console.log("Obteniendo datos productos");

    let xhr = new XMLHttpRequest();

    xhr.onload = () => {
      if (xhr.status === 200) {
        console.log("Peticion completada correctamente");

        console.log(JSON.parse(xhr.responseText));
        this.productos = JSON.parse(xhr.responseText);
        //let APIResponse = JSON.parse(xhr.responseText);
        //this.productos = APIResponse.results;
        console.log(this.productos);
      }
    }

    xhr.open("GET", "http://localhost:8080/hackathon/prestamos");
    xhr.send();

    console.log("Fin de getproduct");
  }
  pushproduct() {
    console.log("pushproduct");
    console.log("Enviando datos productos");

    var data = new FormData();

    data.append('idUsuario', 'persona');
    data.append('idProducto', 'producto');
    data.append('importe', 'importe');

    var xhr = new XMLHttpRequest();
    xhr.open('POST', "http://localhost:8080/apitechu/v2/products/", true);
    xhr.onload = function () {
      // do something to response
      console.log(this.responseText);
    };
    xhr.send(data);
    console.log("Fin de pushproduct");
  }
  deleteproduct() {
    console.log("deleteProduct");
    var url = "http://localhost:8080/hackathon/prestamos";

    var xhr = new XMLHttpRequest();

    //xhr.open("DELETE", url + '/' + idUsuario + '/' + idProducto, true);
    xhr.onload = function () {
      var users = JSON.parse(xhr.responseText);
      if (xhr.readyState == 4 && xhr.status == "200") {
        console.table(users);
      } else {
        console.error(users);
      }
    }
    xhr.send(null);
  }
}

customElements.define('test-api', TestAPI);
